const express = require('express');
const expressLayouts = require('express-ejs-layouts');
const mongoose = require('mongoose');
const flash = require('connect-flash');
const session = require('express-session');
const passport = require ('passport');
const app = express();
var http = require("http").Server(app);
var io = require("socket.io")(http);
var bodyParser = require("body-parser");
app.use(express.static(__dirname));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

//passport config
require('./config/passport')(passport);

//database configuration
const db = require('./config/keyMongo').MongoURI;

//connect to database
mongoose.connect(db, {useNewUrlParser: true})
 
  .then(() => console.log('MongoDB connected'))
    
  .catch(err => console.log(err));

  var server = http.listen(3000, () => {
    console.log("server is listening on port", server.address().port);
});



//socket bind listen
io.on("connection", socket => {
  console.log("a user has been connected");
});

//EJS
app.use(expressLayouts);
app.set('views', __dirname + '/views');
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'ejs');

//bodyparser
app.use(express.urlencoded({ extended: false}));

//express session 
app.use(
    session({
      secret: 'secret',
      resave: true,
      saveUninitialized: true
    })
  );

//passport middleware
app.use(passport.initialize());
app.use(passport.session());

//connect flash
app.use(flash());

//global variables
app.use((req, res, next) => {
    res.locals.success_msg = req.flash('success_msg');
    res.locals.error_msg = req.flash('error_msg');
    res.locals.error = req.flash('error');

    next();
});

//routes
app.use('/', require('./controller(routes)/index'));
app.use('/users', require('./controller(routes)/users'));
app.use('/contacts', require('./controller(routes)/contacts'));
app.use('/images', require('./controller(routes)/images'));
app.use('/messages', require('./controller(routes)/messages'));
app.use('/profile', require('./controller(routes)/profile'));