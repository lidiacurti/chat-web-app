# Chat Web App


Sample project of a web chat application. The users must be registered and logged in to send messages. They also must know the username of other users to start chatting with them.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

This is what you need to install.

```
Node.js
```
We used VS Code to develop the application, but you can use any IDE you want.

### Installing

Step 1:

```
$ npm install
```
Step 2:

To make the server run:

```
$ npm start
```
If you use nodemon:

```
$ npm run dev
```

## Built With

* [Node.js](https://nodejs.org/en/)
* [MongoDB Atlas](https://www.mongodb.com/) - The database used 
* [Express.js](https://expressjs.com/) - The web framework used
* [Flash.js](https://github.com/betaWeb/flashjs) - Framework to handle HTML flash messages
* [Bcrypt.js](https://www.npmjs.com/package/bcryptjs) - Used to hash password
* [EJS](https://ejs.co/) - Used to display the HTML
* [Jquery](https://jquery.com/) 
* [Multer](https://github.com/expressjs/multer) - Middleware used to upload files
* [Mongoose](https://mongoosejs.com/) - Used to handle mongodb with node.js
* [Passport.js](http://www.passportjs.org/) - Authentication middleware

