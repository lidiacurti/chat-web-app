const mongoose = require('mongoose');

const ProfilePicSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    img: {
        data: Buffer,
        contentType: String,
        required: false
    },
    date: {
        type: Date,
        default: Date.now
    }
});

const ProfilePic = mongoose.model('profilePic', ProfilePicSchema);

module.exports = ProfilePic;