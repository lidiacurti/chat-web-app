const mongoose = require('mongoose');

const MessageSchema = new mongoose.Schema({
    sender: {
        type: String,
        required: true
    },
    receiver: {
        type: String,

        required: true
    },
    message: {
        type: String,

        required: true
    },
    img: {
        data: Buffer,
        contentType: String,
        required: false
    },
    date: {
        type: Date,
        default: Date.now
    }
});

const Message = mongoose.model('message', MessageSchema);

module.exports = Message;