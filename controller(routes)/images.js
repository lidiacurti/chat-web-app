const express = require('express');
const router = express.Router();
const multer = require('multer');
var fs = require('fs');
var path = require('path');
const Message = require('../DBModels/Message');


//defining the path of the image that needs to be uploaded
var storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'public/uploads');
    },
    filename: (req, file, cb) => {
        console.log(file);
        cb(null, file.originalname);
    }
});
var upload = multer({ storage: storage });


// Uploading the image from the chat
router.post('/:contact', upload.single('image'), (req, res, next) => {
    //take the image from the correct path
    var img = fs.readFileSync(req.file.path);
    //encode the image in base64
    var encode_image = img.toString('base64');
    const file = req.file;
    const myname = req.user.name;
    const findname = req.params.contact;

    if (!file) {
        const error = new Error('Please upload a file');
        error.httpStatusCode = 400;
        return next(error);
    }
    //create new message with image field
    var newMessImg = new Message({

        sender: myname,
        receiver: findname,
        message: ' ',
        img: {
            data: new Buffer(encode_image, 'base64'),
            contentType: req.file.mimetype
        }
    })
    //put the new message in the Message model on the database
    newMessImg.save()
        .then(message => {
            //call the flash msg
            console.log("image uploaded");
            res.redirect('/chat/' + findname + '/' + myname);
        })
        .catch(err => console.log(err));
})


module.exports = router;

