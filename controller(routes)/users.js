const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const passport = require('passport');
const multer = require('multer');
var fs = require('fs'); 
var path = require('path'); 
var ProfilePic = require('../DBModels/ProfilePic');


//User model
const User = require('../DBModels/User');
//defining the path of the image that needs to be uploaded
var storage = multer.diskStorage({ 
    destination: (req, file, cb) => { 
        cb(null, 'public/uploads'); 
    }, 
    filename: (req, file, cb) => { 
    console.log(file);
    cb(null, file.originalname);
    } 
    }); 
    var upload = multer({ storage: storage }); 

//Login page
router.get('/login', (req, res) => res.render( 'login'));

//Register page
router.get('/register', (req, res) => res.render( 'register'));

//Register handle
router.post('/register',upload.single('profilePic'), (req, res) => {
    const{name, email, password, description, phoneNumber} = req.body;
    let errors = [];

    //Check if all fiels are filled
    if(!name || !email || !password || !description || !phoneNumber){
        errors.push({
            msg: 'Please fill in all fields.' 
        });
    }

    //Check if password's length is ok
    if(password.length < 4){
        errors.push({
            msg: 'Password should be at least 4 characters.'
        });
    }

    //Check if password has at least one number
    var hasNumber = /\d/;
    if(hasNumber.test(password) === false){
        errors.push({
            msg: 'Password should have at least one number.' 
        });
    }

    //Check if phone number is valid
    var numbers = /^[0-9]+$/;
    if(!phoneNumber.match(numbers))
      {
        errors.push({
            msg: 'Write a valid telephone number.' 
        });
      }

    if(errors.length > 0) {
        res.render('register', {
            errors,
            name,
            email,
            password,
            description,
            phoneNumber
           
        });

    
      
    } else {
        //Check email
        User.findOne({ email: email})
            .then(user => {
                if(user){
                    //User already registered with this email
                    errors.push({msg: 'Email is already registered'});
                    res.render('register', {
                        errors,
                        name,
                        email,
                        password,
                        description,
                        phoneNumber
                        
                    });
                } else{
                    //Check username
                    User.findOne({ name: name})
                        .then(username => {
                            if(username){
                                //Username already exists
                                errors.push({msg: 'Username is already registered'});
                                res.render('register', {
                                    errors,
                                    name,
                                    email,
                                    password,
                                    description,
                                    phoneNumber
                                   
                                });
                            }
                            else{
                                
                                //upload profile pic
                                var pic = fs.readFileSync(req.file.path);
                                var encode_image = pic.toString('base64');
                                const file = req.file;

                                if(!file){
                                    const error = new Error('Please upload a file');
                                    error.httpStatusCode = 400;
                                    return next(error);
                                }
                                var pic = new ProfilePic({ 
                                    name: name,
                                    img :{data:  new Buffer(encode_image, 'base64'),
                                    contentType: req.file.mimetype}
                            
                            }  )
                         
                                    pic.save()
                                   .then(profilePic => {
                                    console.log("profile pic uploaded");
                                })
                                .catch(err => console.log(err));
                                
                                //if all is correct, create new user in the database
                                const newUser = new User({
                                    name,
                                    email,
                                    password,
                                    description,
                                    phoneNumber
                                });
                                
                                //Hash password
                                bcrypt.genSalt(10, (error, salt) => bcrypt.hash(newUser.password, salt, (err, hash) =>{
                                    if(err) throw err;
            
                                    newUser.password = hash;
            
                                    //save user
                                    newUser.save()
                                        .then(user => {
                                            //call the flash msg
                                            req.flash('success_msg', 'You are registered! You can log in');
                                            res.redirect('/users/login');
                                        })
                                        .catch(err => console.log(err));
                                }))    
                            }
                        })
                    
                }
            });
            
    }
});

//Login handle
router.post('/login', (req, res, next) => {
    passport.authenticate('local', {
        successRedirect: '/contacts',
        failureRedirect: '/users/login',
        failureFlash: true
    })(req, res, next);
});

//Logout handle
router.get('/logout', (req, res) => {
    req.logout();
    req.flash('success_msg', 'You are logged out');
    res.redirect('/users/login');
})

module.exports = router;