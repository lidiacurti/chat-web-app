const express = require('express');
const router = express.Router();
const { ensureAuthenticated } = require('../config/auth');
const Message = require('../DBModels/Message');
var ProfilePic = require('../DBModels/ProfilePic');

//go to welcome page
router.get('/', (req, res) => res.render('welcome'));

//go to the chat page with the contact selected
router.get('/chat/:contact/:name', ensureAuthenticated, (req, res) => {
    var name = req.params.name;
    var contact = req.params.contact;

//query to find the messages between the user and the contact
    Message.find(
        { $or: [{ $and: [{ sender: name }, { receiver: contact }] }, { $and: [{ sender: contact }, { receiver: name }] }] },
        { _id: 0, __v: 0 },
    )
        .sort({ _id: 1 })
        .exec(function (err, result) {
            if (err) {
                res.send(err);
            } else {
                var messages = result;
                //query to look for the user's profile pic
                ProfilePic.findOne({
                    name: name
                },
                    { _id: 0, __v: 0 },
                )
                    .exec(function (err, result) {
                        if (err) {
                            res.send(err);
                        } else {
                            var imgname = result.img;
                            //query to look for the contact's profile pic
                            ProfilePic.findOne({
                                name: contact
                            },
                                { _id: 0, __v: 0 },
                            )
                                .exec(function (err, result) {
                                    if (err) {
                                        res.send(err);
                                    } else {
                                        var imgcontact = result.img;
                                        //go to chat by passing the messages and profile pics
                                        res.render('chat', {
                                            imagename: imgname,
                                            imagecontact: imgcontact,
                                            message: messages,
                                            contact: req.params.contact,
                                            name: req.params.name
                                        });
                                    }
                                })
                        }
                    })
            }
        })
});
//delete all messages from the selected chat for both user and contact
router.get('/chat/delete/:contact/:name', ensureAuthenticated, (req, res) => {
    var name = req.params.name;
    var contact = req.params.contact;

    Message.deleteMany(
        { $or: [{ $and: [{ sender: name }, { receiver: contact }] }, { $and: [{ sender: contact }, { receiver: name }] }] },
        function (err, result) {
            if (err) {
                res.send(err);
            } else {
                res.redirect('/contacts');
            }
        });

});

module.exports = router;