const express = require('express');
const router = express.Router();
const { ensureAuthenticated } = require('../config/auth');
const Message = require('../DBModels/Message');
const User = require('../DBModels/User');
var ProfilePic = require('../DBModels/ProfilePic');

//this route looks for contacts associated with who's logged in(if there are any)
router.get('/', ensureAuthenticated, (req, res) => {
    var name = req.user.name;
    var contact = [];

    //query to look for contacts
    Message.find(
        { $or: [{ sender: name }, { receiver: name }] },
        { _id: 0, message: 0, date: 0, __v: 0 },
        { sender: 1, receiver: 1 }
    )

        .exec(function (err, result) {
            if (err) {
                res.send(err);
            } else {

                result.forEach(function (message) {
                    if (message.sender == name) {
                        if (!contact.includes(message.receiver)) {
                            contact.push(message.receiver);
                        }
                    }
                    else if (message.receiver == name) {
                        if (!contact.includes(message.sender)) {
                            contact.push(message.sender);
                        }
                    }
                })
            }
        });
    //query to find for the profile pic of the user logged in
    ProfilePic.findOne({
        name: name
    },
        { _id: 0, __v: 0 },
    )

        .exec(function (err, result) {
            if (err) {
                res.send(err);
            } else {
                //render the view contacts and passes the required parameters
                res.render('contacts', {

                    result: result.img,
                    contact: contact,
                    name: req.user.name,
                    description: req.user.description
                });
            }
        })

});

//route used to look for the contact searched by the user logged in
router.post('/find', ensureAuthenticated, (req, res) => {
    const myname = req.user.name;
    const findname = req.body.findname;
    if (!findname) {
        req.flash('error_msg', 'Insert a name');
        res.redirect('/contacts');
    }
    else {
        User.findOne({ name: findname })
            .then(user => {
                if (user) {
                    res.redirect('/chat/' + findname + '/' + myname);
                }
                else {
                    req.flash('error', 'Username does not exist');
                    res.redirect('/contacts');
                }
            })
    }
})

module.exports = router;