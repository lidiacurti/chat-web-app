const express = require('express');
const router = express.Router();
const Message = require('../DBModels/Message');
const app = express();
var http = require("http").Server(app);
var io = require("socket.io")(http);

//upload in the database the text message
router.post("/", async (req, res) => {
  try {
    var message = new Message(req.body);
    var savedMessage = await message.save();

    //send the message to socket to make it visualizable
    io.emit("message", req.body);
    res.sendStatus(200);
  } catch (error) {
    res.sendStatus(500);
    return console.error(error);
  } finally {
    console.log("message post called");

  }
});

//This route looks for new messages in the last 3 minutes. 
//The time's an estimate based on how long it could take to upload images and videos.
router.get('/get/:contact/:name', (req, res) => {
  var name = req.params.name;
  var contact = req.params.contact;
  var endDate = new Date();
  var startDate = new Date(endDate - 60000 * 3);
  Message.find(
    {
      date: { $gte: startDate, $lt: endDate },
      $or: [{ $and: [{ sender: name }, { receiver: contact }] }, { $and: [{ sender: contact }, { receiver: name }] }]
    },
    { __v: 0 },
  )
    .sort({ _id: 1 })
    .exec(function (err, result) {
      if (err) {
        res.send(err);
      } else {
        console.log(result);
        res.send(result);
      }
    });
});

module.exports = router;