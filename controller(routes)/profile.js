const express = require('express');
const router = express.Router();
const multer = require('multer');
var fs = require('fs');
var path = require('path');
var ProfilePic = require('../DBModels/ProfilePic');
const User = require('../DBModels/User');

//defining the path of the image that needs to be uploaded
var storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'public/uploads');
    },
    filename: (req, file, cb) => {
        console.log(file);
        cb(null, file.originalname);
    }
});
var upload = multer({ storage: storage });


// Uploading the new profile pic
router.post('/', upload.single('image'), (req, res, next) => {
    //take the image from the correct path
    var pic = fs.readFileSync(req.file.path);
    var name = req.user.name;

    //encode the image
    var encode_image = pic.toString('base64');
    const file = req.file;
    if (!file) {
        const error = new Error('Please upload a file');
        error.httpStatusCode = 400;
        return next(error);
    }

    //variable of what needs to be updated
    var pic =
    {
        img: {
            data: new Buffer(encode_image, 'base64'),
            contentType: req.file.mimetype
        }
    }
    
    //query to look for the profile pic and update it
    ProfilePic.findOneAndUpdate({ name: name }, pic)
        .then(profilePic => {
            console.log("image uploaded");
            res.redirect('/contacts');
        })
        .catch(err => console.log(err));
});

//to update the user's description
router.post('/description', (req, res) => {
    var user = req.user.name;
    var updateDesc = req.body.updateDescription;

    User.findOneAndUpdate({ name: user }, { description: updateDesc })

        .then(user => {
            console.log('description updated');
            res.redirect('/contacts');
        })
        .catch(err => console.log(err));
});


module.exports = router;